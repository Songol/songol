import tensorflow as tf
keras, L = tf.keras, tf.keras.layers

def build_model(input_shape):
    model = keras.models.Sequential()
    model.add(L.InputLayer(input_shape))
    model.add(L.Conv2D(filters=32, kernel_size = [3, 3], activation = 'relu'))
    #my_model.add(L.Dropout(0.2))
    model.add(L.MaxPooling2D(pool_size=(2,2)))
    model.add(L.Conv2D(filters=64, kernel_size = [3, 3], activation = 'relu'))
    model.add(L.Dropout(0.2))
    model.add(L.MaxPooling2D(pool_size=(2,2)))
    model.add(L.Conv2D(filters=128, kernel_size = [4, 4], activation = 'relu'))
    model.add(L.Dropout(0.25))
    model.add(L.Flatten())
    model.add(L.Dropout(0.2))
    model.add(L.Dense(70, activation='relu'))
    model.add(L.Dense(35, activation='softmax'))
    #sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    #optimizer_snatch=keras.optimizers.Adam(lr=0.0001, beta_1=0.99, beta_2=0.9999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
    #model.summary()
    return model
