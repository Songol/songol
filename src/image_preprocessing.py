import numpy as np
import cv2 as cv
import os

classes_names = np.array(['А', 'Б', 'В', 'Г', 'Д',
                              'Е', 'Ж', 'З', 'И', 'К',
                              'Л', 'М', 'Н', 'О', 'П',
                              'Р', 'С', 'Т', 'У', 'Ф',
                              'Х', 'Ц', 'Ч', 'Э', 'Ю',
                              'Я', '1', '2', '3', '4',
                              '5', '6', '7', '8', '9'])


def label_convertation(labels):
    Y = np.zeros((labels.shape[0], len(classes_names)), dtype=np.uint8)
    classes_names_dict = {}
    for i in range(classes_names.shape[0]):
        classes_names_dict[classes_names[i]] = i
    for i in range(Y.shape[0]):
        current_vector = np.array([])
        for symbol in labels[i]:
            current_symbol = np.zeros(35)
            current_symbol[classes_names_dict[symbol]] = 1
            current_vector = np.append(current_vector, current_symbol)
        Y[i] = current_vector
    return Y


def image_preprocessing_gray(image): #cutting image by numpy and make it gray
    image = image[:, 125:275] #cutting image
    image = cv.cvtColor(image, cv.COLOR_BGR2GRAY).reshape(image.shape[0], image.shape[1], 1)
    return image


def image_segmentation_simple_cut(image):
    image = image_preprocessing_gray(image)
    symbol_width = image.shape[1] // 6
    X = np.zeros(shape=(6, image.shape[0], symbol_width, 1))
    X[0] = image[:, 0:symbol_width]
    X[1] = image[:, symbol_width:symbol_width*2]
    X[2] = image[:, symbol_width*2:symbol_width*3]
    X[3] = image[:, symbol_width*3:symbol_width*4]
    X[4] = image[:, symbol_width*4:symbol_width*5]
    X[5] = image[:, symbol_width*5:symbol_width*6]
    return X
