from sklearn.model_selection import train_test_split
import load_train_data
import pandas as pd
import neural_network


def trained_model(data):
    X_train, y_train = load_train_data.build_dataset(data)
    model = neural_network.build_model(X_train[0].shape)
    model.fit(X_train, y_train, batch_size=64, epochs=20, verbose=1)
    return model