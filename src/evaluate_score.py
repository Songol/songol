import pandas as pd
from sklearn.model_selection import train_test_split
from train_model import trained_model
import numpy as np
from load_train_data import build_dataset

def captcha_score(y_pred, y_true):
    y_pred = np.argmax(y_pred, axis=1).reshape(-1, 6)
    y_true = np.argmax(y_true, axis=1).reshape(-1, 6)
    captcha_metrics = np.array_equal(y_pred, y_true) / len(y_pred)
    return captcha_metrics


data = pd.read_csv('../train_data/map.tsv', sep='\t', header=None)
data.columns = ["FileName", "label"]

data_train, data_test = train_test_split(data, test_size=0.2)
model = trained_model(data_train)
X_test, y_test = build_dataset(data_test)
y_pred = model.predict(X_test)
print(f"test score for model is {captcha_score(y_pred, y_test)}")